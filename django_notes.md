Django Notes
==
#### Separaton Of Concerns
- #### MVC vs MTV
	- Web Development generally follow the Separations Of Concerns concept when talking about the Backend. They consist of three major parts:
    ~~~
 		1.) Database layer
		2.) Code Logic layer
		3.) Presentation layer
~~~
	- Both, the standard MVC as well as Django's MTV approach follow the S.O.C., albeit in a different way. 

##### MTV (Django)		vs. 		Standard
	Models	 	- 	~/Models		(same)
	Templates 	-	~/Views
	Views		-	~/Controller	


#### HTTP request flow in Django
- <strong> Browser:</strong> user types a url
- <strong>project/urls.py:</strong> the management app URLconf processes the URL and forwards it.
- <strong>app*/urls.py:</strong>(usually) to an app URLconf, which forward it to
- <strong>app*/views.py:</strong> the associated Python function in your code logic, and onward to
- <strong>app*/templates/app/template.html:</strong> (Usually) the template file that your function renders.
- <strong>Browser:</strong> finally the rendered page gets displayed to the user who requested it. 

		* - app denotes the name of the APP that you are creating.
### views.py
	- <em>views.py</em> is what contains the <b>code logic</b> of your webapp.
	- <b>view function</b> has to return an HttpResponse or raise an exception such as 404.
	- <b> view function</b> can: 
		- read records from DB or not. 
 		- Use a template system (Django's or third-party template system.)
		- generate a PDF file, output XML, create a ZIP file on the fly
		- Use whatever libraries you want

##### Attaching an html page to views.py 

```
from django.shortcuts import render

# Create your views here
def index(request)
	return render(request, 'blog*/index.html')
```

#### Register Your APP
* Go to settings.py
	- in the <b>INSTALLED_APPS</b> list, add a string with your <em>app-name</em>


