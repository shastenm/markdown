<center>
Python QT Basics
==
</center>

- GUI - Graphical User Interface
	- _Common_ libraries for GUIs in Python include Tkinter, wxPython, PyQt, and PySide.
- Getting to know PyQt
	- PyQt is a Python binding for Qt which is a set of C++ libraries and development tools _providing_ platform independent abstractions for the GUI.
		- Also, _provides_ tools for networking, threads, regular expressions, SQL Databases, SVG, OpenGL, XML, and many other _powerful_ features.
- PyQt's latest additions are:
	- PyQt5: built against Qt5
	- PyQt6: built against Qt6

- PyQt6 provides _classes_ and _tools_ for GUI creation, XML handling, network communication, regular expressions, threads, SQL databases, web browsing and other technologies available in Qt.
	- _Implements_ binding for many Qt classes in a set of Python modules which are organized in a top-level Python package called PyQt6.
		- PyQt6 uses Python 3.6.1 or later.
		- _Compatible_ with Windows, Unix, Linux, Mac, iOS, and Android.

- #### NOTE: Qt Company has developed and currently maintains its own Python binding for Qt library and is the official Qt for Python.
 	- It's Python package is called PySide
	- PyQt and PySide are both built on top of Qt.
		- APIs are similar because they reflect the Qt API.
		- _Importing_ PyQt code to PySide can be as simple as updating _some_ imports
		- If you learn one of them, you'll be able to work with others with minimal effort.
- #### PyQt Installation
	- Several Linux distros include binary packages for PyQt6 in their repos.

\	\	\	\	\	Ubuntu:

			$ sudo apt install python3-PyQt6
\

- ### Creating First Application (Hello World With PythonQt6)
1. _Import_ QApplication and all the required widgets from PyQt6.QWidgets.
2. _Create_ an instance of a QApplication.
3. _Create_ your application's GUI
4. _Show_ application GUI
5. _Run_ application's event loop or main loop.

\

### Step 1


- _Create_ new file _hello.py_.
```
import sys
from PyQt6.QtWidgets import QApplication, Qlabel, QWidget
``` 
- _sys_ allow you to handle app's termination and exit status with the _exit()_ function. 

\

### Step 2

- Create an instance of QApplication
	- You _should_ create your own app instance before you create _any_ GUI object in Python 

```
app = QApplication([])
```

- _Internally_, the QApplication class deals with _command-line_ arguments, and that's why to pass in a _list_ of _command-line_ arguments to the _class constructor_.
- #### NOTE: _Some_ developers pass _sys.argv_ to the _constructor_ of QApplication.
	- This object contains a list of _command-line_ arguments passed into a _Python script_.
		- If your app needs to accept _command-line_ arguments, you should use the _sys.argv_ to handle them.