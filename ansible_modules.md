  <center>
Ansible Modules
==
</center>


![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_ed7fb923-9444-4624-ae19-ba6eef4b702f.jpeg)

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_bd49acd7-ec05-4869-bc49-e886fd345dd2.jpeg)


![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_5ad5f91f-6a00-4348-af3a-e33c5b93b67d.jpeg)

- System Modules are modules pertaining to the Linux System

\


![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_2adf8b21-01b9-4546-8f4b-838a9c69ae82.jpeg)

- Commands Module pertain to shell scripting and bash commands.

\

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_35201df5-0e10-46cc-afdf-af432da618b6.jpeg)

- Files Module pertains to file manipulation
 
	- Acl to set and retrieve acl information

	- Archive/Unarchive to compress and unpack files.

	- Find, Lineinfile, Replace modify contents of an existing file.﻿

\

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_9fdfe06b-2a31-4a10-befe-b88a8b7fbc51.jpeg)

  

- The Database Module helps work databases such as Mongodb Mysql, Postgresql to add/remove dbs or modify them in some way.

\

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_72435c53-bcfe-44ca-b2a8-e1324b880924.jpeg)

  

- The Cloud Module has in depth collections for all the major cloud services such as Amazon, Azure, Cloudstack, Google, etc.
	*   Many modules and options available to many different tasks.

\

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_9d8583cb-cb86-4ddb-b45c-7080e1bf805a.jpeg)

- The Windows Module helps you use Ansible in a Windows environment.
	- A lot more Windows modules that can be found on the Ansible website.

### Let's take a look at a few modules and learn how to read the documentation.

\

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_8a08a44e-798b-41fe-9d85-b7db046afbfe.jpeg)

*   Command module


![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_8d776d4b-0f19-42d9-8100-b94829e791c2.jpeg)

*   The documentation of the command module

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_5001430e-9d31-443a-bd96-f2c41c8c794e.jpeg)

- An example of ansible command module in an actual playbook.yml file.

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_4046d142-7214-4fa4-993e-fc8694e03ddb.jpeg)

- The bash commands are the free\_form input
- The Ansible module parameters are parameter inputs

	*   Not all Modules support Modules like this such as the copy module which only takes parameterized input.
		*   Copy requires src parameter

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_7e68d049-a1ca-4cb3-a368-3e81352861c9.jpeg)

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_7d6a9703-1cb3-474d-a130-763d1e47876f.jpeg)

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_b1b162cc-2447-49bd-847a-a5b8be8dbde9.jpeg)

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_25d52365-9d4b-4ed3-af90-2c2e35dd5780.jpeg)

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_9e02dcb7-47bc-4337-8235-c8102030e1fe.jpeg)

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_20bbcbcb-a9ef-473b-a9de-e71a7ce55bd8.jpeg)

*   You can write the playbook in a dictionary or in map format as shown above

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_d8d4e74f-e924-457c-94c5-a1783f339f44.jpeg)

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_3d076cc0-309e-47b0-a3c1-97c7168e7675.jpeg)

Adding a few more services

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_b9806bf3-df73-49d2-85fd-050403ccc043.jpeg)

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_0da777a5-dad8-402c-8127-8232ec16aa98.jpeg)

*   idempotency - when making multiple identical requests has the same effect as making a single request – then the REST API is called idempotent.
	*   majority of modules are idempotent, and Ansible highly recommends this.
		*   Idea is that Ansible should be able to run the same playbook again and again, and have Ansible report that everything is in an expected state.
		*   If it's not, Ansible's responsibility is to modify it to an expected state.

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_3fc1c34f-6bc9-4d30-aee3-e58684ffc2af.jpeg)﻿

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_a09907c9-1fb1-4010-82d0-979d9b435548.jpeg)

\

### Compare it with a simple Bash script that tries to achieve the same results.

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_5907c9ba-48b2-4252-a9ef-60e012164abe.jpeg)

*   If the script is run multiple times, it adds an entry multiple times.
	*   If you run the Ansible playbook multiple times, it will ensure that there is only a single entry into the file.
		
		- Meaning, _Ansible_ will run the file multiple times, but it will be as though it was only run once.

