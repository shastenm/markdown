<center>
Javascript
==
</center>
\

### _Variables_: containers for storing data.
    
\   \ - Two Steps for making a variable are _declaration_ and _assignment_.

\   \   \   \ Declaration - var, let, const

\   \   \   \ Assignment is made with the assignment operator

\   \   \   \   \   'let' is the best practice for scoping purposes when starting out.

\   \   \   \   \   \   \   \ _Example A:_   

\   \   \   \   \  ```
                    
                    let age;
                    age = 21;
                    console.log(age);

\   \   \   \   \  ```


\   \   \   \   \   \   \   \ _Example B:_
                    
                    let age = 21;
                    console.log(age);

\   \   \   \   \  ```

\   \   \   \   \   \   \ - _Example A_ and _Example B_ do the same thing.

\   \   \   \   \   \   \   \ _Example C:_ (Maybe the user has a birthday)
                    
                    let age = 21;
                    age = age + 1
                    console.log(age);

\   \   \   \   \  ```

\   \   \   \   \   \   \   \ - Age is updated to 22.
 
\

\

\

### _Strings:_ a series of characters.

    ```
    let firstName = 'Tuxer';
    console.log(firstName);
    ```

### _boolean expressions_: returns True or False

\   \   - if true, student is enrolled

\   \   - if false, student graduated, no longer enrolled, or was never enrolled.

\   \   \   \ ```

\   \   \   \ let student = true;

\   \   \   \ ```

\   \ You could combine all of the above examples.

\   \   \   \ ```

\   \   \   \ console.log("Hello", firstName);

\   \   \   \ console.log("You are", age, "years old.");

\   \   \   \ console.log("Enrolled:", student);

\   \   \   \ '''

\   \ If you want to show this in the DOM:

### HTML Document:

```

<body>
    <p id="p1"></p>
    <p id="p2"></p>
    <p id="p3"></p>
</body>
```

### Javascript file

```

document.getElementById("p1").innerHTML = "Hello " + firstName;
document.getElementById("p2").innerHTML = "You are" + age;
document.getElementById("p1").innerHTML = "Enrolled:" + student;

```

\

\

## _Math_

## _Arithmetic Expressions_ is a combination of _operands_(values, variables, etc), operators (+ - * / %) that can be _evaluated_ to a value.

    ```

let students = 20;

students = students +1;

console.log(students);

let extraStudents = students % 3;

console.log(students)

students += 1;

    ```

### _Augmented Assignment Operators_
(+=, -=, *=, /=)

### Operator Precedence
\   \   1. Parenthesis

\   \   2. Exponents

\   \   3. Multiplication and Division

\   \   4. Addition and Subtraction

\

_Examples:_

```

let result = 1 + 2 * (3 + 4); => 15
                1 + 2 * 7
                  1 + 14
                    15

```
let result = (1 + 2) * (3 + 4); => 21

                 3   *   7
                     21
```


\

\

## JS Input

* _How to accept user input_

    * Easy Way:

    ```
    
    let username = window.prompt("What's Your Name? ");
    console.log(username);

    ```

    * Hard Way:

HTML file:

    ```
    <body>
        <label>Enter your name: </label><br>
        <script src="index.js"></script>
        <input type="text" id="myText"><br> 
        <button type="button" id="myButton">Submit</button>
    </body>
    ```



Inside js file:

```

let username;

document.getElementById('myButton').onclick = function() {

\    \   username = document.getElementById("myText").value;

 \   \   console.log(username);   
}

```

