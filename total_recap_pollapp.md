![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_3741be38-e3ec-4f2d-8cee-3e8e05a04aaa.jpeg)

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_003e6fe1-79b4-4e76-a6ca-6f1e0a328f70.jpeg)

Writing Django Models
---------------------

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_5386d8c6-b020-4128-a28c-25661c9a5df2.jpeg)

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_78d7e1ac-7f9a-47f9-a208-d817d36590bf.jpeg)

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_795bd405-2132-44d3-863d-84234348ebcb.jpeg)

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_d6db9fdd-bec0-44a9-95ba-8f17af64ad5c.jpeg)

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_dcbff721-3efe-4f6b-99ce-6d7e74be60ec.jpeg)

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_b75bf760-7fea-4bca-8167-748e56c38953.jpeg)

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_e451f553-9a07-4bae-960a-b8f105044dd7.jpeg)

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_5b093260-9a6d-4088-93e2-3e0859f5eb70.jpeg)

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_2d2f39cb-791e-4dda-b1a5-d479f3c37359.jpeg)

Django's Generic Views
----------------------

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_f40837ac-d7ba-48a7-b116-d13a50ae0bae.jpeg)

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_a4278586-cb52-41ab-996c-822c1cd05874.jpeg)

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_3acf549f-a56d-422d-892c-616fd44a8ae1.jpeg)

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_a5d39306-c0d4-46ca-8ce2-f0fb75024b28.jpeg)

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_56fd0f9a-0ecc-497c-b372-70d75a69ee6a.jpeg)

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_f91b4160-f643-420c-9fad-9f7cbc63cbce.jpeg)

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_f3fd151f-6922-47be-ac91-14a3cb075107.jpeg)

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_784456fc-7da2-47ec-9a82-59d29761cc76.jpeg)

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_122b292c-c9f3-4f78-9fa8-c66ff2d3bf86.jpeg)

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_14131900-13bc-490c-9e4d-e0da5166e15c.jpeg)
