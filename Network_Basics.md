Networking Basics
==
* Switching
* Routing
* Default Gateway
* DNS Configuration Linux

\

<center>
### Network For Switch
\	\	\ <h3>Host A 	\	\	\	\	\	\ 	\	\	\	\	\	\	\	\	\	\	\	\	\	\	Switch \	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	Host B</h3>

192.168.1.10 \	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\ 	\ 192.168.1.0  \	\	\	\	\	\	\	\	\	\	\	\	\	\ 	\	\	\	\	\	\	\ 	\ 192.168.1.11
</center>

\

\

> ### Switch - creates a _network_ that contains _two systems._

\

To connect them, you need an _interface_ on each host.

(Choose ip address for eth0 for ethernet)

~~~
$ ip link
~~~

To connect the hosts to the switch:

~~~
# Host A
$ ip addr add 192.168.1.10/24 dev eth0
# Host B
$ ip addr add 192.169.1.11/24 dev eth0
~~~ 

Check if the switch is working (from Host A; ping ip address of Host B):
```
$ ping 192.168.1.11
```

\

\

> ### Routing - used to connect _two networks_ together

\

<center>
### Network A	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\ Network B
Host A	\	\ -	\ -	\ -	\ -	\	\	\ Switch\	\ -	\ -	\ -	\ -	\	\	\ Host B	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\ Host C	\ 	\ -	\ -	\ -	\ -	\ -	\ 	\ Switch	\	\ -	\ -	\ -	\ -	\ -	\	\ Host D

192.168.1.10 \	\	\	\	\	192.168.1.0 \		\	\	\	\ 192.168.1.11 \	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	192.168.2.10 \	\	\	\	\	\	\	\ 192.168.2.0 \		\	\	\	\	\	\	 192.168.2.11	

\ | 	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\|

 192.168.1.1 \	\	\	\ -	\ -	\ -	\ -	\ -	\ -	\ -	\ >	\	\	\ Router	\	\ <	\ -	\ -	\ -	\ -	\ -	\ -	\ -	\ -	\	\	\	\	\	\	\	\	\	\	192.168.2.1
</center>

\

- the _router_ gets 2 ip's assigned (_one_ on each network).

- Another way to look at routers is to imagine a _server_ with many network ports.

\


> ### Gateways

\

### When System B is trying to send a message to System C, since the router is just another device on the network and there could be many _such_ devices. So, how does it know where the router is? Gateways.

- Systems can only reach systems _within_ the same network (192.168.1.0 or 192.168.2.0).

	- if the network is a _room_, the gateway is a _door_ to the outside world, to _other_ networks, _or_ to the internet.
		- _systems_ need to know where that _door_ is.
- To see the _existing_ routing configuration on a system
		
	- _display_ the existing kernel routing table
```
$ route
```
	- Configure a _gateway_ for one _system_ to reach the _system_ on another _network_.
```
$ ip route add 192.168.2.0/24 via 192.168.1.1
```
	- _Now_, when you run the _route_ command, it should show the new gateway.
		- This has to be configured on ALL _systems_.
		- If System C is to _send_ a packet to System B
		```
		$ ip route add 192.168.1.0/24 via 192.168.2.1
		$ ip route add network via router
		```
\

 
<center>
### Internet
172.217.194.0

\|

### Network A	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\ 	\ 	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\ Network B
Host A	\	\ -	\ -	\ -	\ -	\	\	\ Switch\	\ -	\ -	\ -	\ -	\	\	\ Host B	\	\	\	\	\	\	\	\	\	\ 	\ 	\|	\	\ 	\	\	\	\	\	\	\ Host C	\ 	\ -	\ -	\ -	\ -	\ -	\ 	\ Switch	\	\ -	\ -	\ -	\ -	\ -	\	\ Host D

192.168.1.10 \	\	\	\	\	192.168.1.0 \		\	\	\	\ 192.168.1.11 \	\	\	\	\	\	\  	\ 	\ 	\	\ 	\ |	\	\	\	\	\	192.168.2.10 \	\	\	\	\	\	\	\ 192.168.2.0 \		\	\	\	\	\	\	 192.168.2.11	

\ | 	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\|	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\|

 <h3>192.168.1.1	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	Router 1	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	192.168.2.1</h3>

\  	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\|	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\|

<h3>\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	Router 2	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	192.168.2.2</h3>


(Adding Router 2)

</center>

\

- If the _systems_ need access to _Google_ IP
```
$ ip route add 172.217.194.0/24 via 192.168.2.1
``` 
- Instead of _manually_ entering IP addresses that you can access, you can set it to _default_.
	- for _any_ network you don't know the IP of, route it to a specific router. 
		- Both of these _examples_ mean the same thing.
```
$ ip route add default via 192.168.2.1
$ ip route add 0.0.0.0 via 192.168.2.1
```

- If you have 0.0.0.0 in the _gateway field_, it means that you don't need a _gateway_.


#### Default Gateway

- If you have multiple routers in your _network_
		
	- 1 for the _internet_
	- 1 for the _private internal network_
	- 2 separate _entries_ for each network
```
$ ip route add default via 192.168.2.1
$ ip route add 192.168.1.0/24 via 192.168.2.2
```

	- Now, when we _display_ the kernel routing table
```
$ route
Kernel IP Routing Table
Destination			  Gateway
 default 			192.168.2.1  	-	(internet)
192.168.1.0			192.168.2.2		-	(private internal network)
```

\

> ### How To Set-up A Linux Host As A Router

\


<center>
System A	\	\	\	\	\	System B	\	\	\	\	\	System C

\	\	\	\	\	192.168.1.0	\	\	\	192.168.2.0	\	\	\	\	

eth0 	\	\	\	\	\	\	 eth0 	\	\	\	\	\ eth1	\	\	\	\	\	\	\	\	eth0 \	\	\	\

192.168.1.5	\	192.168.1.6	\	192.168.2.6	\	192.168.2.5

/	\	\	\	\	\	\	\	\	\	\	\\	\
\	Gateways
</center>

\


### How Do I Get System A To Talk To System C?

- If I try to _ping_ System C from System A, it won't work by _default_.
	- To change that, you just enter routing tables for _each_ system.

	```
	$ ip route add 192.168.2.0/24 via 192.168.1.6
	$ ip route add 192.168.1.0/24 via 192.168.2.6
	``` 
	- Now, when I can ping the system, but we won't get a response

- By default, packets are NOT forwarded _from_ interface _to_ another in Linux.
	- packets on system B eth0 are **NOT** _forwarded_ through to eth1 for _security_ reasons.
		- say eth0 is connected to a _private network_ and eth1 is connected to a _public_ one. We **wouldn't** want anyone from the _public network_ to be able to _send_ messages to the _private network_ unless you wanted the ability to do so.
		- to _explicitly_ give eth0 and eth1 the _ability_ to send messages to each other.

		```
# default it is disabled
$ cat /proc/sys/net/ipv4/ip_forward
	 return 0
# to enable it 
$ echo 1 >> /proc/sys/net/ipv4/ip_forward
		```
(This will not be persistent.)
- In order to have the _ability_ upon Reboot (or _make_ it persistent).
```
$ sudo vi /etc/sysctl.conf
. . .
net.ipv4.ip_forward=1
. . .
```

\
\

> ### Conclusion

\

- Used to _list_ and _modify_ interfaces on the host
```
$ ip link
```
- used to see the ip address _assigned_ to which interface
```
$ ip_addr
```
- used to _set_ the ip addresses on the interfaces
```
$ ip_addr add
```

- These won't be persistent
		
	- to _make_ persistent modify /etc/network_interfaces file

- View the routing table (either one)
```
$ ip route
$ route
``` 
- Used to _add_ entries into the _table_
```
$ ip route add network_ip via router_ip
```


