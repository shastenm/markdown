<center>
Ansible Variables
==
</center>  

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_ec1f5a83-1eb0-4bf3-8fe9-6b2c1e908184.jpeg)

*   Variable _stores_ information that _varies_ with each host or _different_ items.
	*   If you want to apply patches to 100s of servers, only a _single_ playbook is necessary.
	*   Variables store information about the different hostnames, usernames, or passwords that are different for each server.

\

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_83cdd13a-5e19-4a03-90b5-14b28de24a65.jpeg)

*   _Examples of variables are ansible\_host and ansible\_connection_.
	*   We can also use the playbook itself to _list_ variables with the _vars_ parameter followed by the key-value format.
	*   We can also have _variables_ stored in a separate file dedicated to storing variables.

\

  ![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_d04d9527-a37a-4702-a667-bb20b6e01338.jpeg)



### How Do We Use Variables?

* ### To _use_ a variable simply enclose it inside {{ my\_var }}.

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_6d03b1ea-f730-4c7a-948d-c58517df210f.jpeg)

*   This is a playbook _used_ to set multiple firewall configurations with a number of values _hardcoded_ in it.
	*   If someone wanted to reuse _this_ _playbook_, they would have to modify these values (see below).

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_98b5b796-91f1-4c6c-8ba6-a6327d589e89.jpeg)


*   There are a _couple_ of **solutions**:
	*   If these _values that are prone to change_ in the _playbook_ are transferred to the _inventory file_, and the _variables_ are called inside the playbook.
		*   This allows us to luxury of only having to modify the inventory file in the future.

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_339430d9-ae12-4e66-8f21-cd0d77783546.jpeg)

*   An _even_ better **solution**:
	*   Move the variables into a file in the name of the host.

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_a79be596-2570-42bc-84b0-fb1ad2c80add.jpeg)

- The format that is _used_ is Jinja2 Templating {{ }}.

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_e4ee75f6-44e3-4cd2-bdd5-d214ba6d8773.jpeg)

*   Note how to use these _variables_ inside and outside of strings, and how they are very similar to f-strings in Python inside the string.