API's and Databases
==
### Request Types

#### GET 
	- The GET method requests a representation of the specified resource.   
		- Requests using GET should only retrieve DATA
		- Read DATA from the server
#### POST
	- Used to submit an entity to the specified resource, often causing a change in the state or side effects on the server.
		- Create DATA on the server
#### PUT

- replaces all current representations of the target resource with the request payload.
		- PUT and PATCH are often used interchangeably.
 		- Update data on the server

#### PATCH

    - is used to apply partial modifications to a resource.   

#### DELETE

	- Deletes a specified resource.

#### HEAD

	- asks for a response identical to that of a GET request, but without the response body.

#### OPTIONS

	- used to describe communication options for the target resource.
		- Used to get information about your communication options with the server.

#### CONNECT

	- establishes a tunnel of the server identified by the target resource.
#### TRACE

	- performs a message loop-back test along the path to the target resource.
		- Just basically used to make sure the connection is good and active.

### Requests Library

#### Install requests with pip


```
import requests

base_url = "http"//demo.codingnomads.co:8080/tasks_api/users"
request = requests.get(base_url)
```
