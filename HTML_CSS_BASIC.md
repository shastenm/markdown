HTML

  

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_f889168a-ab3c-4665-a50e-c25bb82c4e49.jpeg)

  

  

*   Access the web faster with the robots attribute in the meta tag.
*   You can put an icon in the tab.
*   Display an image

  

![Example of how to set aspect ration of photo in HTML](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_f20632eb-14d9-413f-9653-df8783c21dc2.jpeg)



\

\

\

\

\

\

\

\

![Example of how to display a video in HTML](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_fabfd6e0-7038-4f31-8b7a-508db3e4d81f.jpeg)

\

*   Use the controls attribute to be able to play, scroll, full-size, etc video.
*   Use the autoplay attribute to be able to have the video autoplay. Useful for having a video in the background or something.

  

![Example of nesting a the source in video tag](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_d8208a06-6c81-41eb-ae2d-79f4fa1dc603.jpeg)


\

\


Forms
-----



![Basic idea for how to write-out a form](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_1f4bce04-d3f6-48bf-a7e2-e5419d748f88.jpeg)

  

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_bce43a09-0820-4be4-b64e-19154c520bd1.jpeg)

  

  


  

![Adding additional text fields and button](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_811ec308-8ef4-42ca-a7c9-84de282dd377.jpeg)

  

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_6521ee8c-156f-401f-9507-c33a8e03ae6d.jpeg)

  

  

*   explicitly choosing the "email" as a type with make sure the email address has an '@' symbol in the address.

\

\

\

\

\

\

\

\

\

\

\

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_be08e169-96d1-4f0f-bb45-3314ad15b280.jpeg)


![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_068d23ad-b47b-4c97-93f0-811418f4835c.jpeg)

  

  

*   You can have characteristics for your _input_ tag such as _maxLength_ and _minLength_
*   You can use the _select_ tag to have a drop-down menu of items.
*   _textarea_ tag gives a little box for the text area.

\

\


**Tables**
  

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_48d98913-1334-47c8-9a9b-fe620ad145f7.jpeg)

  

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_ac57bce7-ba55-41c6-a5c4-85a61a9e9887.jpeg)

  

  

*   tr - table row
*   td - table data
*   th - table header

\

\

**Classes and ID's**

*   Main _difference_ between an id and a class is that the id name is only used once, and a class name can be reused.
*   It's also possible to have two different classes linked to one tag.
*   <p class="article-text active">

  

**Tags with semantic value**

  

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_7e01a434-7347-4b10-81a7-0b59c95f774a.jpeg)

  

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_dd1df4d1-1f3a-4023-a52f-e2239dafaf81.jpeg)

  

  

*   header tag - top section of the website
*   nav tag - navigation bar
*   section tag - separates a main section
*   figure tag - used wrap elements like images.
*   figcaption tag - adds captions to image or element.
*   footer tag - bottom section of the website
*   aside tag - side bar section
*   article tag - similar to section tag except it needs to make sense on it's own like a blog post or an article.
*   main tag - wrapper for entire page minus header and footer tags

**HTML5 Element Characters**

  

*   used in place of characters that will break the code

  

===

css - Cascading Stylesheets
===========================

  

  

*   You can simply use the tag globally (h1{ })
*   indicates that you want to use a certain .class (.some-class{ })
*   indicates a tag inside an element (.some-class h1{ })
*   indicates that you want to use an #id (#some-id{ })
*   indicate that you want to use a tag under an element (.some-class > li{ })
*   indicate you want to use a tag inside an element (.some-class li{ })
*   afftects whole page (\* { })

  \

  \

**Indexing**

  
*   index through an element .some-class:nth-child(1){ }
*   index a tag inside an element .some-class li:nth-child(2){ }

**Margin And Padding**

  

*   margin: top left bottom right;
*   padding top left bottom right;

  

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_ee5e68af-a862-4ad6-9955-887405b2195b.jpeg)

*   You can remove any external margins and padding.

**Font**

  

*   font-size: 1em;
*   1em = 16px
*   em multiplies by the parent element
*   you can use em with any characteristic like padding or margin.
*   font-size: 1rem;
*   rem doesn't inherit nothing from the parent element
*   You can set the font-size to 62.5%, and that way each rem will be 10px. By default, it is set to 16px.

  

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_f26938f7-5d53-49fb-be5a-fdece6d9f7aa.jpeg)

  

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_58daf470-4741-48ed-922a-59ba371a4582.jpeg)

  \

  \

*   Make the button a circle

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_239d5e12-87fc-4a77-ac25-f6458def3589.jpeg)

  

**Display**

  

*   display:block;
*   has a line break
*   display:inline;
*   doesn't have a line break
*   cannot add height or width to inline elements, but you can use margin.
*   display:inline-block;
*   combination of block and inline.
*   respects height and width parameters unlike the inline eleme

  

  

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_7bcaa54c-d1bc-43cf-9116-b4d8188c8c76.jpeg)

  

  

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_0452bd09-99be-4518-8afb-b18d690baca8.jpeg)

  

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_50561e02-03c3-48ab-b66e-2b29366f34f4.jpeg)

\

\

\

\

*   Default position

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_f858a76d-479d-4b35-98e1-00f4f0dd20cc.jpeg)

  

  

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_ce2590dd-237e-452e-88a0-9221f9ac4f7e.jpeg)

  

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_ed12f335-f005-471b-9c55-afca2bf747a9.jpeg)

  

  

*   Purple pops out of place and is placed over top, blue and black squares are pushed-up.

  

  

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_69640a6c-779f-46b2-904a-8701c8c434d4.jpeg)

  

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_3594a974-c321-4e6c-bcaa-ad55fb7d2f08.jpeg)

\

\  

Bottom Left Position

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_20520fb8-f4e7-4b12-acce-b120b8bb9634.jpeg)

  

Top Right Position

  

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_b2ff1dc5-c804-4a68-a55c-663ec0fa90c3.jpeg)

  

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_b394e4fd-cf73-4273-981a-e364a01b8308.jpeg)

  

Right Side Position

  

*   Lowered top by 20 rem

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_fc9da00e-d540-4037-bbbb-edf61c46d59d.jpeg)

  

Center Position

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_ce4e175e-a60a-485e-aabb-cfb4cae7279e.jpeg)
![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_fc5caa2e-4be8-49a3-a118-8620b6aaadad.jpeg)

Relative Position

  

*   box1 class relative to container class which keeps object inside container class

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_35f47f3e-7b5b-49b8-ac2b-e7a88fdb61c0.jpeg)

  

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_a02b89f3-9bab-4ead-bc86-91454f67538a.jpeg)

  

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_1b2b5b89-7fe1-4384-b77e-eb75de397ec7.jpeg)

  

  

*   Moves 3rem from the original position when starting inside a nested element.

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_62468fd4-3e63-40a8-a0b1-352d78cfdc4e.jpeg)

  

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_5ade0a87-7272-4af2-b4b8-2359418c9189.jpeg)

  

Fixed Position

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_9fd323d5-d341-40bb-a196-4e935c2d9b3a.jpeg)

  

  

*   Position stays fixed doesn't move.

  
\

\

Sticky Position

  

*   Nested element stays in position while you scroll down the page

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_04e71df6-6726-4970-88c9-d6af8d50cb41.jpeg)

  

Pseudo Classes

  

*   hover

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_d94055ec-1a89-4b86-83c1-bb39efa5c51f.jpeg)

  

  

*   focus (changes when clicked)

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_872c877f-6e0a-4f7b-a1d4-f1fc76ce7072.jpeg)
