<center>
Developer's Time Organizer
==

<h6><u>To Do</u> \		\	\ <u>Bugs</u> \		\	\ <u>Goals</u> \	\	\	<u>Schedule</u></h6>
</center>

<center>
To Do Lists
==
<h6><u>Today</u> \	\ <u>This Week</u>	\	\ <u>This Month</u>	\	\ <u>This Quarter</u>	\	\ <u>Later</u></h6>
</center>

<center>
Bugs
==

<h6><u>Low Priority</u>	\	\ <u>Medium Priority</u> \	\ <u>High Priority</u> \	\ <u>Critical Priority</u></h6>
</center>

<center>
Goals
==
<h6><u>1-4 Weeks</u> \	
<u>1-2 Months</u> \	
<u>3-6 Months</u> \	
<u>6-9 Months</u> \	

<u>9-12 Months</u> \	<u>12-18 Months</u> \	<u>18-24 Months</u>	\ <u>Later</u></h6> 
</center>

<center>
Schedule
==
<h6><u>Today</u> \ 
<u>This Week</u> \ 
<u>Next Week</u> \ 
<u>3-4 Weeks</u> 

<u>1-2 Months</u> \ 
<u>3-4 Months</u> \ 
<u>Later</u></h6>
</center>


\

\

\

> <h3> To Do List</h3>

- <h3>Make a _checklist_ App</h3>
	- The _checklist_:
		- be able to take multi-step notes to detail how I am going to do something.
		- be able to _set_ the _priority_ (imperative, important, moderate, low).
		- be able to _set_ the _deadlines_.
	 	- have an _add_ button.
			- All forms will be made with YAML files.
		 - have _add_ contact button that _stores_ name, phone number, address, and email.
 numbers_ and _emails_ and _encrypt_ them.
			 - They will be decrypted on the host machine with your ssh public key
 

	- when the _box_ in the _list_ gets _checked_, there appears _delete_ and _finished_ buttons.
		- if the _finished_ is pressed, it is removed from the _list_ and placed at the end of the line of a text file.
		- counter += 1 \	\	\	finished_jobs += 1
	- At the end of the month/year,a _graph_ and _report_ will show me how well I am staying on task.
		- Everything that are below _medium priority_ count as things _completed_, and will not add anything to the _counter variable_.
		- _print(f"{**counter**}")_
		 - _print(f"{**finished_jobs**}")_
		 - _print(f"{**finished_jobs / counter**}")_
	 - _name look-up_ feature that _lists_ the _collection_ of individual _contacts_.
	 - set reminders to browser
	 - _side-window_ that lists 
		- the next 3 things to get done 
		- the 3 most critical bugs listed
	 	-  the next 3 high things scheduled for that day
		- next 3 goals of highest priority. 
	
\

\

> <h3> Bugs</h3>


- <h3>Make a _checklist_ App</h3>
	- The _checklist_:
		 - take multi-step notes to give details of the bug.
		 - set _priority_ (low, medium, high, critical).
		 - set _deadline_ to fix bug.
		 - have an _add_ button
			- All forms will be made with YAML files
		- add _contact_ where it will store the name and email of the person who made the bug report.
			- will be decrypted on the host machine using your ssh public key
	- when the _box_ in the _list_ gets _checked_, there appears _delete_ and _finished_ buttons.
		- if the _finished_ is pressed, it is removed from the _list_ and placed at the end of the line of a text file.
		- counter += 1 \	\	\	finished_jobs += 1
	- At the end of the month/year,a _graph_ and _report_ will show me how well I am staying on task.
		- Everything that are below _medium priority_ count as things _completed_, and will not add anything to the _counter variable_.
		- _print(f"{**counter**}")_
		 - _print(f"{**finished_jobs**}")_
		 - _print(f"{**finished_jobs / counter**}")_

\

\

> <h3> Goals</h3>

- <h3>Make a _checklist_ App</h3>
	- The _checklist_:
		- take multi-step notes to give details about the goal.
		 - set _priority_ (low, medium, high, critical).
		 - set _deadline_ for the goal (1-4W, 2-6M, 7-12M, 13-18M, 19-24, later).
		 - have an _add_ button
	 		- All forms will be made with YAML files
	- when the _box_ in the _list_ gets _checked_, there appears _delete_ and _finished_ buttons.
		- if the _finished_ is pressed, it is removed from the _list_ and placed at the end of the line of a text file.
		- counter += 1 \	\	\	finished_jobs += 1
	- At the end of the month/year,a _graph_ and _report_ will show me how well I am staying on task.
		- Everything that are below _medium priority_ count as things _completed_, and will not add anything to the _counter variable_.
		- _print(f"{**counter**}")_
		 - _print(f"{**finished_jobs**}")_
		 - _print(f"{**finished_jobs / counter**}")_

\

\

> <h3> Schedule</h3>

- <h3>Make a _checklist_ App</h3>
	- The _checklist_:
		- take multi-step notes to give details about the planned event.
		 - set _priority_ (low, medium, high, critical).
		 - have an _add_ button
	 		- All forms will be made with YAML files
	 - Make certain parts of the app _sync_ with _Google Calendar_.
	- when the _box_ in the _list_ gets _checked_, there appears _delete_ and _finished_ buttons.
		- if the _finished_ is pressed, it is removed from the _list_ and placed at the end of the line of a text file.
		- counter += 1 \	\	\	finished_jobs += 1
	- At the end of the month/year,a _graph_ and _report_ will show me how well I am staying on task.
		- Everything that are below _medium priority_ count as things _completed_, and will not add anything to the _counter variable_.
		- _print(f"{**counter**}")_
		 - _print(f"{**finished_jobs**}")_
		 - _print(f"{**finished_jobs / counter**}")_

\

\

> <h3>Directory App</h3>

- <h3>Make an app of all my _contacts_</h3>
	- list my contacts _alphabetically_ by _last name_.
		- add, edit, and remove buttons
		 - YAML file
		 - Give _server_ my public key, and encrypt the contact list that's on my host machines.

