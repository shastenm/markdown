LINUX
==

- Everything is a file in Linux 
	- Regular File
		- Images, Scripts, Configuration/Data Files, etc
	- Directory
        ```
        /home/bob
        /root
        /home/bob/.config
        ```
- Special Files
	- Character Files (mouse, keyboard)
	- Block Files (partitions, ram)
	- Links (hard and soft links)
	- Socket Files (enables communication between two processes)(systemd, init systems in general)
	- Named Pipes (allow one process connecting as an input to another)

#### Identifying the file type
```
	$ file + directory # Outputs the file-type
Example:
	$ file /home/billy
	/home/billy/: directory
```


BASH
==

#### COMMON COMMANDS

```
$ type (command)			= tells if command is built-in or external
$ echo (arg)				= print statement
$ ls						= list
$ ls -la					= lists hidden files
$ mv 						= moves file from one destination to another
							* also used to rename file(s)	
$ cat						= concatenate
$ cp 						= copy a file
$ cp -r 					= copy a file recursively
$ rm						= remove file
$ rm -rf					= remove file recursively
$ touch						= create a new file
$ cd 						= change directory
$ pwd						= present working directory
$ mkdir 					= make directory
```





### PUSHD AND POPD

```
$ pushd						= bookmarks a directory
$ popd						= reverts back to a bookmarked directory
$ pushd +0					= reverts back to index 0
$ dirs -v					= list indexes of pushd command
```

### PAGERS (MORE LESS)

``` 
$ more some_file.txt
	[Space] - scrolls the display, one screen full of data at a time
	[Enter] - scrolls the display one line at a time
	[b] - scrolls backwards one screen full of data at time
	[/] - search text
	[q] - quits

$ less some_file.txt
	[Up] - scrolls up one line at a time
	[Down] - scrolls down on line at a time
	[/]	- search text
	[q]	- quits
```
