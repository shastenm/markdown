<center>
Jinja 2
==
</center>
> ### About Jinja 2

- Used for _templating_.
		
	- _Hypothetical Situation_: CEO wants to send an _email_ to everyone in the company.
		- the _email_ is the template.
		- the _variables_ are the names and other details

	- A _templating engine_ is responsible for taking a template and _applying_ a given set of variables to it to create a useable output.
		- In IT webspaces, it is commonly used for creating web pages.
			- like, an html page that uses a set of variables and together they can create a custom html page.
  			- Another popular use case is with automation tools such as Ansible.
				- Ansible uses templating extensively to customize playbooks to have custom information or create custom configuration files

	- Jinja 2 is a fully-featured templating engine for Python.

### String Manipulation
	
- The _methods_ are referred to as _filters_ in Jinja2.

		
	- substitute

```

The name is {{ my_name }} => The name is Bond.

```   
	- upper

```
The name is {{ my_name | upper }} => The name is BOND.

```
	- lower
```

The name is {{ my_name | lower }} => The name is bond.

```
	- title
```

The name is {{ my_name | title }} => The name is Bond.

```
	- replace
```

The name is {{ my_name | replace('Bond', 'Bourne') }} => The name is Bourne.

```
	- default
		- If you don't declare the value of the variable, the Jinja2 templating engine will fail with a message that it is _undefined_. Here is a good work-around if it's undefined.

```
	The name is {{ first_name | default("James") }} {{ my_name }} => The name is James Bond.
```



### LIST AND SET BASED FILTERS

- the _minimum_ of a list of numbers:

```
{{ [1,2,3] | min }}  => 1 

```
- the _maximum_ of a list of numbers:

```

{{ [1,2,3] | max }}  => 3

```
- list of _unique_ filters in an array:

```

{{ [1,2,3,4] | unique }}

```
- _Combine_ and _retrieve_ the _unique_ numbers from two arrays:

```

{{ [1,2,3,4] | union([4,5]) }} => [1,2,3,4,5]

```
- _Intersect_ filter to get _common_ numbers in two arrays:

```

{{ [1,2,3,4] | intersect([4,5]) }} => 4

```
- _Random_: generate a random number:
 
```

{{ 100 | random }} => output a random number between 1-100

```
- join - joins an _array_ of words:

{{ ["The", "name", "is", "Bond"] | join(" ") }} => The name is Bond



### Loops

- {% %} _indicates_ that the jinja2 code is a _block_ and not a _one-liner_. 

```
{% for number in [0,1,2,3,4] %}
{{ number }}
{% endfor %}
```



### Conditionals

- _Wrap_ statement within an if/endif block

```

{% for number in [0,1,2,3,4] %}
	{% if number == 2 %}
		{{ number }}
	{% endif %}
{% endfor %}

```
