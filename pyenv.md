Pyenv Commands
==
~~~ 

 $ pyenv --three
  
		  	-This starts pyenv with the version of Python 3 that you have installed.

 $ pipenv shell

			-This starts the virtual environment shell

 $ pipenv install (package)
			-Installs your package with dependencies

~~~

#### WHEN PIPENV WON'T LOCK
~~~
pipenv venv
~~~
- This will list all the virtual environments choose the last one delete it. Just reexecute 
```
pyenv --three
```
and just proceed to make new virtual environment.
 
