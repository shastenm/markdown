MySQL DATABASES
==
To access logs in a Database
``` 
> cat /var/log/mysqld.log
	- This will show a temporary password when you are setting-up MySQL. 
	  You can THEN use that temporary "PASSWORD" to establish a more 
	  permanent password to get access to your DATABASES.
> mysql -u root -pg/$temp_password
```
\

To get a <I>permanent</I> password, it is done with <B>ALTER USER</B> command.
```
> ALTER USER 'root'@'localhost' IDENTIFIED BY '$PASSWORD';
	- If you don't want to use this command, you can opt to use
	a bash command inside of a Linux shell, and not in the MySQL prompt.
		$ mysql_secure_installation
```
\

<b>SHOW DATABASES</b>:
```
mysql> SHOW DATABASES;
```
\

<B>CREATE</B> a <B>DATABASE</B>:
```
mysql> CREATE DATABASE name_of_database;
```
\

Select(<b>USE</b>) a <B>DATABASE</B>:
```
mysql> USE name_of_database;
	- Can only work on one database at a time
```
\

<B>CREATE</B> a <i>table</i>:

\ \ \ \ <I>Example</I>
```
mysql> CREATE TABLE persons
(
Name varchar(255),
Age int,
Location varchar(255)
);  
```
\

<B>SHOW TABLES</B>:
```
mysql> SHOW TABLES;
```
\

<b>INSERT DATA</b> into the <b>TABLE</b>:

\	\ <i>Example</i>
```
mysql> INSERT INTO persons values
("John Doe", 45, "New York");
```
\

View <b>Data</b> in the table
```
mysql> SELECT * FROM persons;
```
\

### Permissions For Accessing Certain Databases 
> In certain situations, you may want to create users in which you restrict their "access" to certain DB's, or you wish to restrict access to the database to certain computers. 

\
Access <b>database</b> from a certain <i>host</i>:
```
mysql> CREATE USER 'john'@'localhost' IDENTIFIED BY '$PASSWORD';
	- Can only connect to the database from one particular host
	- This could be restricted to access solely from a company computer for example. 
```

\

Access <b>database</b> using <i>ip address</i>:
```
mysql> CREATE USER 'john'@'192.168.1.10' IDENTIFIED BY '$PASSWORD';
	- as long as user has the same ip address, they'll be able to access the database.
	- they can connect from remote systems
```

\
Give the ability to access <i>database</i> from <i>ALL</i> systems, use the '%' symbol.
```
mysql> CREATE USER 'john'@'%' IDENTIFIED BY '$PASSWORD';
``` 

\

### Authorize USER ACCESS to a Specific Database
\
```
mysql> GRANT <PERMISSION> ON <DB.TABLE> TO 'john'@'%';
```
\
\	\	<i>Examples</i>
\
\
Using one privilege:
```
mysql> GRANT SELECT ON school.persons TO 'john'@'%';
```
Using more than one privilege:
```
mysql> GRANT SELECT, UPDATE ON school.persons TO 'john'@'%';
```
Assign all of the <i>tables</i> of <i>school</i> Database
```
mysql> GRANT SELECT ON school.* TO 'john'@'%';
```
Give full authorization and permission:
```
mysql> GRANT ALL PRIVILEGES ON *.* TO 'john'@'%';
```
To see a full list of GRANTS assigned to a user:
```
mysql> SHOW GRANTS FOR 'john'@'localhost';
	- Can be useful while troubleshooting
```
\

\
<center>## Privileges 
</center>
\	\	\ ALL PRIVILEGES	\	\	\	\	\	\	\ - 	\	 GRANT ALL ACCESS

\	\	\ CREATE	\	\	\	\	\	\	\ 	\	\	\	\	\	\	\  - 	\	 Create Databases

\	\	\ DROP	\	\	\	\	\	\	\ 	\	\	\	\	\	\	\ 	\	 \ - 	\	 \ Delete Databases

\	\	\ DELETE	\	\	\	\	\	\	\ 	\	\	\	\	\	\	\  - 	\	 Delete rows from table.

\	\	\ INSERT	\	\	\	\	\	\	\ 	\	\	\	\	\	\	\  - 	\	 Insert rows into table.

\	\	\ SELECT	\	\	\	\	\	\	\ 	\	\	\	\	\	\	\  - 	\	 Read/Query tables.

\	\	\ UPDATE	\	\	\	\	\	\	\ 	\	\	\	\	\	\	\  - 	\	 Update rows in table.

