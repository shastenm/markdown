<center>
Docker Command
==
</center>

- _start_ a container.
	- (Below) _Runs_ an instance of nginx on _host_ if it exists; otherwise, it goes to _Docker Hub_ and _pulls_ it down.
		- After the _first_ time going through this, the _same_ image will be reused.
```
 $ docker run nginx
```
<>
\

- _list_ all _running_ containers and some basic information about them.
	- _container ID_, image name, command, current status, and name of the container.
		- _Each_ container _automatically_ gets a random ID and name.
```
$ docker ps
``` 

- _list_ all containers running or not with '-a' flag.
```
 $ docker ps -a
```

\

- _stop_ a container
	- Must provide _either_ container ID or name.
		- If _unsure_ what _name_ or id, run the ps command _first_.
```
 $ docker stop nginx
```

\

- _permanently_ remove a container.
	- Must provide _either_ container ID or name.
	- Must be in a _stopped_ or _exited_ state before attempting to do remove container.
		- It will _print_ the name back if it was _successful_.
```
 $ docker rm nginx
 nginx
```

\

- _list_ all available images and their sizes.
```
 $ docker images
```

\

- _remove_ the image
	- Must _include_ name of the image.
		- _Remember_ to _check_ that no other _containers_ are running off of that _image_ before attempting to remove it.
			- If there are _containers_ running off of the _image_, stop and remove **all** dependent _containers_.
```
 $ docker rmi image_name
```

\

- _pull_ image from _Docker Hub_.
	- This doesn't _run_ the container, _only_ starts it on the host machine.
```
 $ docker pull image_name
```

\

> NOTE: VIRTUAL BOXES ARE MEANT TO RUN AN OPERATING SYSTEM WHEREAS CONTAINERS ARE DESIGNATED TO RUN A SPECIFIC TASK OR PROCESS. IT MIGHT HOST AN INSTANCE OF A WEB-SERVER, APPLICATION SERVER, DATABASE SERVER, OR SIMPLY RUN A SPECIFIC CALCULATION OR ANALYSIS OF SOME SORT. 

- Virtual boxes vs containers: _Virtualbox_ will keep running an OS whereas in a _container_ when the task or process is _complete_, the container exits.
	- The _container_ only lives as long as the process inside it is _alive_.
		- if the _webserver_ inside it is _stopped_ or _crashed_, the container _exits_.
		- This _explains_ why when running an Ubuntu _image_, the container _stops_ immediately after _running_ the docker image because Ubuntu is _just_ an image of an OS that is _used_ as the _base image_ for other applications.
			- There is no _application_ or _process_ running by _default_.
		- If _image_ doesn't _run_ any service (like Ubuntu), you can tell Docker to run a _process_ along with a _command_ to suspend after 5 seconds (**$ docker run ubuntu sleep 5**).
			- When the _container_ starts, it _executes_ the sleep command and goes to _sleep_ for 5 seconds, and then the sleep command _exits_.
		- What if we'd like to run a _command_ while a _container_ is _running_?
```
 $ docker ps -a
``` 
 		- It will show that the image is set to _sleep_ for 100 _seconds_.

\

- If you would like to see the contents _inside_ the container by _exec_ command.
```
 $ docker exec docker_name cat /etc/hosts
 Prints contents of /etc/hosts
```

\

> ### Run Attach and Detach

\

##### Attach mode

```
 $ docker run website/webapp
```
- You will be _attached_ or _connected_ to the _console_ or _standard out_ of the Docker _container_ and you will see the _output_ of the _webservice_ on your screen.
	- You won't be able to do _anything_ else on the _screen_ other than _view_ output until the _container_ stops.
		- CTRL-C: stop the container and application _hosted_ on the container.

\

##### Detach Mode
```
 $ docker run -d website/webapp
```
- _Runs_ the _container_ in background mode and you will have a _prompt_ while it continues in the background.
	- _Run_ ps command to view the _containers_.
	- If you would like to _reattach_ the container later.
		- If _specifying_ the ID of a container, and you can simply provide _first_ few _characters_ alone just so that it is _differentiates_ itself from other containers on the host.
	
```
 # Examples:
 $ docker attach a043d
``` 

	
