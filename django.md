Django
==
- #### Start A Django Project (pyenv shell)
~~~
(django) $ django-admin startproject (name) .
		-the '.' is very important because it keeps everything in one main folder.
		-project name can't have a hyphen that separates words, use an underscore instead.
~~~
- #### Run The Development Server
 ~~~		
	$ python manage.py runserver
        - development server that comes prepackaged with Django
		-serves your webapp on http://localhost:8000
~~~
- #### Make a Django App
~~~ 	
	$ python manage.py startapp NAME-OF-APP 
	$ python manage.py runserver
~~~
-  #### Create a link to App

    - Imports the os module

    ```
    import os
    ```

 	- This tells django where to find the app
		- <b>DJANGO_PROJECT/urls.py</b>

```
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
	path('admin/', admin.site.urls),
	path('NAME_OF_APP/', include('NAME_OF_APP.urls')), 
]
```

    - Add 'static' folder

    ```
    STATIC_URL = 'static/'
    STATICFILES_DIRS = [
        os.path.join(BASE_DIR, 'static'),
    ]
    ```

-  #### Register the app
 	- <b>DJANGO_PROJECT/settings.py</b>
		- Go to <b>INSTALLED_APPS</b> and add:

```
INSTALLED_APPS = [
	'NAME_OF_APP',
]
```

- Make a <b>Templates</b> Folder inside <b>NAME_OF_APP</b> folder
	- make an <b>NAME_OF_APP</b> folder inside <b>Templates</b> folder
		- django is going to read the index.html document inside the NAME_OF_APP folder*
- Make a <b>urls.py</b> document inside <b>NAME_OF_APP</b> folder
- <b>NAME_OF_APP/urls.py</b>
```
from django.urls import path
from . import views

urlpatterns = [
	path('', views.index)
		# this look to the NAME_OF_APP/views.py document and pulls the index method.
]
```

- <b>NAME_OF_APP/views.py</b>
```
from django.shortcuts import render

# Create your views here

def index(request):
	return render(request, 'NAME_OF_APP/index.html')
		# This will look in NAME_OF_APP/templates/NAME_OF_APP/index.html folder
		# You will also put Bootstrap, js, etc inside methods here.
```
		
\
\
\


#### Here's what the file structure looks like

```
- FOLDER
	- APP
		- migrations
		- templates
			- APP
				index.html
		__init__.py
		admin.py
		apps.py
		models.py
		tests.py
		urls.py
		views.py
	- PROJECT
		__init__.py
		asgi.py
		settings.py
		urls.py
		wsgi.py
		db.sqlite3
	-static
		- css	
		- js
		- fonts
		- images
	manage.py
	Pipfile
	{} Pipfile.lock
```

\

#### HTML FILE


    ```
    <!DOCTYPE html>
    {% load static %}  <!--jinga syntax to load the static folder
    ```

- Adds css file that is nested inside the static folder

    ```
    <!--css -->
    <link rel='stylesheet' href="{% static 'css/style.css' %}">
    ```

- Adds an image that is nested inside img folder inside static folder
    
    ```
    {% static 'img/cpu.jpg' %}
     ```

#### SQLITE3

- Start the CLI:

    ```
    $ sqlite3
    ```

- Open with a database file:
    
    ```
    sqlite3>>.open db_name.sqlite3

    ```

- Display the schema of one of the tables:

    ```
    sqlite3>>.schema table_name --indent
    
    ```

- See the content of one of the tables:

    ```
    sqlite3>>SELECT * FROM table_name

    ```

- To exit the CLI:

    ```
    sqlite3>>.exit

    ```


#### Migrations


- After migrating, you will have databases for all those apps inside the settings.py file.
    - Open sqlite3:
    
    ~~~
    sqlite3>>.open db.sqlite3
    sqlite3>>.tables
    ~~~
    
- Create migration files _inside_ the migrations file:  
    
    ```
    $ python3 manage.py makemigrations

    ```

- Apply the apps that are _inside_ the migrations file

    ~~~
    $ python3 manage.py migrate
    ~~~

\

#### models.py class

~~~
class Post(models.Model):
    title = models.Charfield(max_length=140)
    text = models.TextField()
    published = models.DateTimeField(auto_now_add=True)
~~~
\

#### Django ORM (Object Relational Mapper)

- The ORM basically translates between models, which builds up objects and tables which are the relations.
    - It does the _translation_ between the code that you are writing and the instances that get created in the database.
\

#### Using The Django Shell

- With the _virtual environment_ activated.

    ~~~
    $ python3 manage.py shell

    >>> from APP.models import Post #class
    >>> p = post(title="Getting Started", text="I'm writing this post")
    >>> p
    <Post: Post object (None)>
    >>> p.title
    'Getting Started'
    >>> p.text
    "I'm writing this post"
    >>> p.published
    >>>     <-- doesn't return a value because one is not present.
    >>> p.save()    <-- saves the database from object to table.
    >>> Post.objects.all()  <--makes the call
    <QuerySet [ <Post Object (1) >]>
    >>> post = Post.objects.get(id=1)
    >>> post
    <Post: post object (1)>
    >>> post.title
    'Getting started'
    >>> post.published
    datetime.datetime...
    ~~~

- Post.objects.all() - Gets the fetch objects from the database.
    - Have to iterate through the different objects to get the different items inside or you would use the 'get' method.

- Renaming a database value
    
    ~~~
    >>> post.title
    'Getting Started'
    >>> post.title = 'Got started'
    >>> post.save()
    >>> Post.objects.get(id=1).title
    'Got Started'
    ~~~

- Passing Data Through A Context Dictionary

    -APP/view.py

    ~~~
    from django.shortcuts import render
    from . models import Post

    def index(request):
        posts = Post.Objects.all()
        context = { 'posts': posts }
        return (request, 'APP/index.html', context)
    ~~~

- Django Templating Language
    -basically allows you to write Python in html.

    ~~~
    
    {% code logic %} - for loops, if statements, etc.

    {{ variables }} - variables

    {{ variables|filter }} - whenever you pipe a variable, it serves as a filter.
    ~~~

#### Using pk

- Accessing a single entry with pk (primary key)

    ~~~
    >>> from blog.models import Post
    >>> post = Post.objects.all()
    >>> posts
    <Query Set ...>
    
    // The following code basically does the same thing. Kinda.
    // It singles out whatever the primary key is instead of the id tag.

    >>> p1 = Post.objects.get(id=1)
    >>> p1 = Post.objects.get(pk=1)
    ~~~
\
- Capturing Path Components (APP/urls.py)

    ~~~
    urlpatterns = [
        path("int:pk>", views.detail)
    ]
    ~~~

        - http://localhost:8000/blog/1/

 \

- APP/views.py

    ~~~
    def detail(request, pk):
        context = { 'pk': pk }
        return render(request, 'APP/detail.html', context)
    ~~~
    
- APP/templates/APP/detail.html
    
    ~~~
    <p>{{ pk }}</p>
    ~~~

#### Django Admin GUI

~~~
$ python3 manage.py createsuperuser
~~~

-Access is granted using http://localhost:8000/admin

    -Django will ask for a username, email, and password
        - After creating superuser, you can use the credential to login and use the admin interface. 


