DWM Configuration
==
**Common Tasks**

Logout			-		M-A-Esc

Keyboard Layout -		M-A-l

Inc/Dec Opacity	-		A-t,y	

Clipboard		-		M-c 

Dmenu			-		M-S-Enter 

Terminal		-		M-Enter

Toggle bar		-		M-b

Fullscreen		-		M-S-f

Floating Window	-		M-f

Close Window	- 		M-x

**Set Volume Controls**

Decrease Volume	-		A-j

Increase Volume	-		A-k

Mute Volume		-		A-m

Volume Sink		-		A-n	 	

**Behavior**

Rotate Stack	-		M-S-j,k

Focus Stack		-		M-j,k

Horizontal/		-		M-i,o
Vertical			

Resize Layout	-		M-h,l

Zoom(switch)	-		M-s

Set Layouts		-		M-t,y,u

Set Layout		-		M-space

Switch Prev		-		M-Tab

Kill Client		-		M-x

Focus Monitor	-		M-period, comma

Move Node To	-		M-S-period, comma
Other Monitor

Gaps			-		M-minus, equal
						M-S-equal
