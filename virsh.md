
# Virsh Reboot

=======================


```
virsh net-undefine default
virsh net-destroy default
virsh net-list
systemctl enable --now libvirtd
systemctl enable virtlogd.socket
systemctl restart libvirtd.service
cat default.xml
virsh net-define default.xml
virsh net-autostart default
virsh net-start default

```

