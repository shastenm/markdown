Python Basic String and Data Types 
==

#### DATA TYPES
```
str			- 		string
int			-		integer
float, complex
list, tuple, range
dict		-		dictionary
set, frozen-set
bool		-		boolean
byes, bytearray, memoryview
NoneType
```

#### ESCAPE CHARACTERS
```
\'			-		single quote
\\			-		backslash
\n			-		new line
\r			-		carriage return
\t			-		tab
\b			-		backspace
\f			-		form feed
\ooo		-		octal value
\xhh		-		hex value
```

#### STRING SLICING
```
string[from:to:interval]
```
#### STRING FORMAT
```
Ex.1
var = 10
text = "I'm {var} years old."
print(text.format(var))

Ex. 2
var1 = 0
var2 = 3
var3 = 6
string = {2}, {0}, {1}
print(string.format(var1, var2, var3))
```


