SQL LITE
==
#### Start the <em>sqlite</em> CLI:
```
$ sqlite3
```
#### Open up a <em>database</em> file:
```
sqlite3>> .open YOUR_DB_NAME.sqlite3
```
 #### Show ALL tables in a database:
` ``
sqlite3>> .tables
```
## ## Display the schema of one of the tables:
``` 
sqlite3>> .shema YOUR_TABLE_NAME --indent
```
####  See the content of one of the tables. This is <em>not</em> a dot-command, but a pure SQL query. You can write any SQL queries in the <i>sqlite3</i> CLI:
``` 
sqlite3>> SELECT * FROM YOUR_TABLE_NAME;
``` 
#### To exit the CLI:
```
sqlite3>> exit
```
List Overview of Most Important Dot-Commands
--
* <b>Remember</b> you need to be inside the <em><b>sqlite3</em></b> CLI to run these commands

	* .open DB_FILE_NAME
	* .tables
	* .schema TABLE_NAME
	* .exit
