# $$\text{My mason.nvim Configuration}$$ 
___________________________________________________________
\

leader = ','

CTRL-n: \   Toggle Tree 


\
| **LSP Config**

leader + rn:    \   Rename buffer

leader + co:    \   Code actions 

leader + gd:    \   Rename buffer

leader + gi:    \   Implementation

leader + gr:    \   Lsp references

leader + rn:    \   Hover over code parts

\
| **Telescope**

CTRL-p: \   \   Find files

leader-leader:  \   \ Old files

leader-fg:  \   \ Live grep

leader-fh:  \   \ Help Tags


\
| **cmp settings**

CTRL - b:   \   Scrolls up.

CTRL - f:   \   Scrolls down.

CTRL - o:   \   Scrolls previous docs.

CTRL - e:   \   Scrolls down by line.

\

\

\
| **Knap Markdown Live Preview**

F5: \   Processes document once and refreshes

F6: \   Closes viewer application and resets settings 

F7: \   Toggles the auto-processing on and off 

F8: \   SyncTeX forward search

\

\
| **Terminal**

leader-;    \   \   Toggle open and close terminal window

leader-leader-(+/-)    \   \ Increase/Decrease window width 

leader(+/-)    \   \ Increase/Decrease window height 

leader-(1-5);    \   \ Keymaps to open nth terminal


