SSL & TLS BASICS
==
<H6>\	\	\	\	\	TLS	\	\	\	\	\	\ - \	\	\	\	**Transport Layer Security**</h6>
<H6>\	\	\	\	\	SSL	\	\	\	\	\	\ - \	\	\	\ **Secure Sockets Layer**</h6>

\

> <h3>Certificates</h3>

-	**What is the purpose of a certificate?**
		
	- To guarantee authenticity between _two_ parties during a transaction. 	 		

-	**General Process Of Accessing Server With TLS**
		
	- _user_ tries to _access_ the _server_.
		a. **TLS certification** _ensures_ the communication between the user and the _webserver_ is **encrypted** and that the server is who it says it is.

- **Why Do You Need Encryption?**
	- without _encryption_, a user's _information_ can be accessed by a hacker by simply finding where the **text file** is _located_.  

- **What Is Encryption?**
	- Encryption is where you randomly replace the alphabet with letters and numbers, and you then reprint the content of a text file with the content using the encrypted alphabet.
			
		a. An _encryption key_ is the process replacing the _alphabet_ with random letters and numbers. 

- **What is decryption?**
		
	- Decryption happens after your encrypted information gets to the server and you need the original information in it's original form. There exists a key/lock mechanism to encrypt and decrypt.
 
- **How Does Encryption Basically Work?**
	- An _algorithm_ makes a _key_, and replaces the regular _alphabet_ of a text file a person wishes to encrypt with the newly formed alphabet.
	- The _file_ is then sent to the _server_.
		a. The _idea_ is that a hacker could be sniffing data from the _website_ and could get the _encrypted data_ only they are unable to _decrypt_ it without the _key_.
		b. A potential problem with this is when a _copy_ of the _key_ is sent to the _server_ over the same _network_ resulting in the hacker getting the _key_ and the _encrypted_ document.
			1. This is mainly the problem with _symmetric encryption_.

\

> <center> <h3> The Two Types Of Encryption</h3>
<p>_Symmetric And Asymmetric Encryption_</p> 
</center>  

\

### \	\	\	\ Symmetric Encryption

\

- **Symmetric Encryption** is where the same _key_ is used to _encrypt_ and _decrypt_ a _file_.
		
	- This means that the _key_ has to be _exchanged_ between the _sender_ and _receiver_.
	- Even though, this is considered a secure way of encryption, when used by itself it poses some serious _security threats_ because the hacker now potentially has your _key_ and the _encrypted_ document; which means he/she has _access_ to your personal data.

- **Asymmetric Encryption ** is where the _user_ uses a _pair_ of _keys_ to secure their _data_ which consist of a _public key (lock)_ and a _private key_. 
		
	- The _idea_ is that if you _lock_ the _encrypted_ data with your _public key_ that you can ONLY use the _private key_ to _decrypt_ the _data_.
		- A _private key_ is stored on the _host machine_, and shouldn't be _shared_ with <u>ANYONE</u>.
		- A _public key_ is stored on the _server_ side, and there's NO _danger_ with people _accessing_ the _public key_ because it can only _LOCK_ the _data_.

- #### Sounds nice in theory, but how does it work if I have a server and wish to use Asymmetric Encryption instead of a password?





  


