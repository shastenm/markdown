DATATYPE Methods
==
> <h3>STRINGS</h3>

\


<b>CHAR MANIPULATION			STRING INDEX   			STRING SPLITTING			BOOL METHODS																		STRING JUSTIFY	
--							--					--						--																					--	
	capitalize()						find()				split()				isalnum()																		center()
	casefold()							lfind()				lsplit()		isalpha()																				ljust()
	lower()								rfind()				rsplit()		isdecimal()																			rjust()
	maketrans()							index()				splitline()			isdigit()																			<b>PARTITION</b>
	replace()				<b>FORMAT STRING</b>     	<b>STRIP STRINGS</b>										isidentifier()													partition()																			
	swapcase()						format()			strip() 		islower()																					lpartition()
	title()							format.map()		lstrip()			isnumeric()																								rpartition()
	translate()						f-strings 			rstrip()		isprintable()																										<b>OTHER</b>
	upper()						<b>CONCATENATE			<b>PREFIX/SUFFIX</b>																isspace()																												count()					
									join()			startswith()												istitle()																							endcode()
															endswith()			isupper()																					expandtabs()
												:echo b:knap_viewer_launch_cmd																												zfill()

\
\
\
\

> <h3>LIST/ARRAY METHODS</h3>

\	

<h5>	\	\	\	\	\	\	\ METHOD	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	DESCRIPTION</h5>---
<b>	\	\	\	\	\	\	append()</b> \	\	\ 	\	\	\	\	\	\	\ <b>adds</b> <i>element</i> to the end of a <i>list</i>. \n

<b>	\	\	\	\	\	\	\ clear()</b>\	\ 	\ 	\	\	\	\	\	\	\	\	\	<b>removes</b> <i><u>ALL</u></i> elements from the <i>list</i>.	\n

<b>	\	\	\	\	\	\	\ copy()</b>\	\	\ 	\	\	\	\	\	\	\	\	\	\ <b>returns</b> a <b>COPY</b> of the <i>list</i>.

<b>	\	\	\	\	\	\	count()</b>\	\	\ 	\	\	\	\	\	\	\	\	\ 	<b>returns</b> the <b>number</b> of elements with the specified <i>value</i>.

<b>	\	\	\	\	\	\ extend()</b>	\	\	\ 	\	\	\	\	\	\	\	\ <b>add</b> <i>elements</i> or <i>iterables</i> to the end of a <i>current list</i>.

<b>	\	\	\	\	\	\	index()</b> 	\	\ 	\	\	\	\	\	\	\	\	\  <b>returns</b> the <i>element</i> at a specified <i>position</i>.

<b>	\	\	\	\	\	\	insert()</b> 	\	\ 	\	\	\	\	\	\	\	\	\  <b>adds</b> an <i>element</i> at the specified <i>position</i>.

<b>	\	\	\	\	\	\	\	pop()</b> 	\	\ 	\	\	\	\	\	\	\	\	\  \ <b>removes</b> the <i>element</i> at the specified <i>position</i>.

<b>	\	\	\	\	\	\	remove()</b> 	\	\ 	\	\	\	\	\	\	\  <b>removes</b> the <b>first</b> <i>item</i> with the specified <i>value</i>.

<b>	\	\	\	\	\	\	reverse()</b> 	\	\ 	\	\	\	\	\	\    \  <b>reverses</b> the <i>order</i> of the <i>list</i>.

<b>	\	\	\	\	\	\	\	sort()</b> 	\	\ 	\	\	\	\	\	\	\	\	\  <b>sorts</b> the <i>list</i>.

\
\

> <H3>DICTIONARY METHODS</H3>

\
<h5>	\	\	\	\	\	\	\ 	\ METHOD	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	DESCRIPTION</h5>___
<b>	\	\	\	\	\	\	\	clear()</b> 	\	\ 	\	\	\	\	\	\	\	\	\  <b>removes</b> <i><u>ALL</u></i> of the elements from the <i>dictionary</i>.

<b>	\	\	\	\	\	\	\	\ copy()</b> 	\	\ 	\	\	\	\	\	\	\	\	\  <b>returns</b> a <i>COPY</i> of the <i>dictionary</i>.

<b>	\	\	\	\	\	\	\	fromkeys()</b> 	\	\ 	\	\	\	\	\	 \ <b>returns</b> a <i>dictionary</i> with the specified <i>keys</i> and <i>value</i>.

<b>	\	\	\	\	\	\	\	\ 	\ get()</b> 	\ 	\	\	\	\	\	\	\	\	\  \  \ <b>returns</b> the value of the specified <i>key</i>.

<b>	\	\	\	\	\	\	\	\ items()</b>	\	\ 	\	\	\	\	\	\	\	\	\ <b>returns</b> a <i>list</i> containing the <i>dictionary keys</i>.

<b>	\	\	\	\	\	\	\	\	\ pop()</b> 	\	\ 	\	\	\	\	\	\	\	\	\  <b>removes</b> of an <i>item</i> with a specified <i>key</i>.

<b>	\	\	\	\	\	\	\	\ popitem()</b> 	\	\ 	\	\	\	\	\	\  <b>removes</b> the <i>last</i> <u>inserted</u> <b>key-value</b> pair.

<b>	\	\	\	\	\	\	\	\ setdefault()</b> 	\	\ 	\	\	\	\	 <b>returns</b> the <i>value</i> of the specified <i>key</i>; if it doesn't <i>exist</i>: <b>insert</b> the <u>key</u> with 
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 	\	\	\	the specified <i>value</i>. 

<b>	\	\	\	\	\	\	\	\ update()</b> 	\	\ 	\	\	\	\	\	\	\	<b>updates</b> <u>dictionary</u> with the specified <i>key-value</i> pairs.

<b>	\	\	\	\	\	\	\	\ value()</b> 	\	\ 	\	\	\	\	\	\	\	\	\ <b>returns</b> a <i>list</i> of all the <i>values</i> in the <i>dictionary</i>.

\

\

> <H3>TUPLE METHODS (IMMUTABLE LIST)</H3>

\

<h5>	\	\	\	\	\	\	\	\	METHOD	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	DESCRIPTION</h5>___

<b>	\	\	\	\	\	\	\	count()</b> 	\	\ 	\	\	\	\	\	\	\	\	\  <b>returns</b> the <i>number of times</i> a special value <i>occurs</i>.

<b>	\	\	\	\	\	\	\	index()</b> 	\	\ 	\	\	\	\	\	\	\	\	\  <b>searches</b> the <b>tuple</b> for a specified <i>value</i> and <b>returns</b> the position of where it was <i>found</i>.


\

\

> <H3>SET METHODS</H3>

\

<h5>	\	\	\	\	\	\	METHOD	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	\	DESCRIPTION</h5>___

###	\	\	\	 METHODS USED TO MANIPULATE SETS
<b>	\	\	\	\	\	\	add()</b> 	\	\ 	\	\	\	\	\	\	\	\	\  <b>adds</b> an element to the <i>set</i>.

<b>	\	\	\	\	\	\ clear()</b> 	\	\ 	\	\	\	\	\	\	\	\	\  <b>removes</b> <u><i>ALL</i></u> the e
nts from the <i>set</i>.

<b>	\	\	\	\	\	discard()</b> 	\	\ 	\	\	\	\	\	\	\	<b>removes</b> the specified <i>item</i>.

<b>	\	\	\	\	\	\	pop()</b> 	\	\ 	\	\	\	\	\	\	\	\	\  \ <b>removes</b> an element from the <i>set</i>.

<b>	\	\	\	\	\	\	remove()</b> 	\	\ 	\	\	\	\	\	\	<b>removes</b> specified element


### \	\	\	BOOLEAN METHODS
 
<b>		\	\	\	\	\	isdijoin()</b> 	\	\ 	\	\	\	\	\	\	\	<b>returns</b> whethr <u><b>two</b> sets</u> have an <i>intersection</i>.

<b>	\	\	\	\	\	issubset()</b> 	\	\ 	\	\	\	\	\	\  <b>returns</b> whether <u><b>another</b> <i>set</i></u> contains this <i>set</i> or not.

<b>	\	\	\	\	\	discard()</b> 	\	\ 	\	\	\	\	\	\	\	<b>returns</b> whether <b><u>this</b> <i>set</i></u> contains <i>another <b>set</b></i> or not. 

### \	\	\	DIFFERENCE METHODS
<b>	\	\	\	\	\	difference()</b> 	\	\ 	\	\	\	\	\	\	\	\	\	\	<b>returns</b> a <i>set</i> containing the <i>difference</i> between <b>two or more</b> sets.

<b>	\	\	\	\	\	difference_update()</b> 	\	\ 	\	\	  \	<b>removes</b> the <i>items</i> in <b><u>this</b> <i>set</i></u> that are also included in <i>another</i> specified <i>set</i>.

### \	\	\	SYMMETRIC DIFFERENCE METHODS

<b>	\	\	\	\	\	symmetric_difference()</b> 	\	\ 	\	\	\	\	\	\	\	\ <b>returns</b> a <i>set</i> with the <i>symmetric differences</i> of <i>two</i> <b>sets</b>.

<b>	\	\	\	\	\	symmetric_difference_update()</b> 	\	\ 	<b>inserts</b> the <i>symmetric differences</i> from <b>this</b> <i>set and another</i>.

### \	\	\	INTERSECTION METHODS
<b>	\	\	\	\	\	intersection()</b> 	\	\ 	\	\	\	\	\		\	\	\	\	\	\	\	\	\	\	\	<b>returns</b> a <i>set</i> that is <i>intersected</i> with <i><b>two</b> other sets</i>. 

<b>	\	\	\	intersection_update()</b> 	\	\ 	\	\	\	\	\		\	\	\	\	\	\ <b>removes</b> the <i>items</i> in <b>this</b> <i>set</i> that are <b>NOT</b> <i>present</i> in other <i>specified set(s)</i>.

### \	\	\	OTHER METHODS

<b>	\	\	\	\	\	\ copy()</b> 	\	\ 	\	\	\	\	\	\	\	\	\  \ <b>returns</b> a <i>copy</i> of a set.

<b>	\	\	\	\	\	\ union()</b> 	\	\ 	\	\	\	\	\	\	\	\	\  <b>return</b> a <i>set</i> containing the <i>union</i> of <b>sets</b>.

<b>	\	\	\	\	\ update()</b> 	\	\ 	\	\	\	\	\	\	\	\	 \ <b>update</b> the <b>set</b> with the <i>union</i> of this <i>set and others</i>.


