![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_618caf99-113f-4528-b3bf-3a4e4b3b8333.jpeg)

Create A Procfile
-----------------

*   Create Procfile in main folder
*   web: gunicorn APP\_NAME.wsgi

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_1b8496b4-9682-462f-9ced-494956359dc3.jpeg)

Install gunicorn and django-heroku
----------------------------------

*   Install gunicorn and django-heroku with pip

Update requirements.txt file
----------------------------

*   python -m pip freeze requirements.txt


\


Initialize django-heroku
------------------------

*   settings.py file

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_e4e654e3-1c0a-4959-986b-1d94b2a97e2b.jpeg)

![](https://storage.googleapis.com/askify-screenshot/x9BqtZNXaaMwVQ8GXybg53bQeAM2/extension_screenshots/screenshot_default_c90499b0-f4e7-428c-bec9-a9c905ce91b4.jpeg)
