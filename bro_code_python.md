<center>
# Bro Code Python
</center>
### Tuples
- **tuple** is a _collection_ that is _ordered_ and _unchangeable_, and is _used_ to group together _related_ data.
	- Instead of [] like in lists, tuples _use_ parentheses.
		- we can _add_ variables inside those parentheses.
\

#### Example - student record

- Each student has a name, age, and gender.
```
# student is the tuple.
student = ("tuxer", 47, "male")
```

#### Tuple Methods (count, index)

- _Count_ how many _instances_ of a value that _appears_ in a tuple.
```
print(student.count("tuxer"))
> 1
```
- _Print_ the _index_ of a value.
```
print(student.index("male"))
> 2
```

\

#### A Couple Of Tricks Using Tuples

- _Print_ all of the _contents_ in a _tuple_.
```
for s in student
	print(s)
```

- _Check_ if a value _exists_ in the _tuple_.
	- _If_ the statement _is_ **true**, it will run the code. Otherwise, it _will do_ nothing.
```
if "tuxer" in student:
	print("Tux is here")
```

### Sets

- _Sets_ are _unordered_ and _unindexed_ and will not _return_ duplicate values.
	- _Surround_ all values with curly braces. 

#### Example of a collection of silverware that we call utensils.

```
utensils = {"fork", "spoon", "knife"}
```	

- _Display_ **all** values in the set.
```
for u in utensils:
	print(u)
```

#### Set Methods

- _Add_ an element.
```
utensils.add("napkin")			# Adds napkin to the set
``` 

- _Remove_ an element.
```
utensils.remove("fork")			# Removes fork from the set
```

- _Clear_ elements.
```
utensils.clear()				# Removes ALL elements from the list.
```

- _Add_ a set of dishes.
```
utensils = {"fork", "spoon", "knife"}
dishes = {"bowl", "plate", "cup", "knife"}
```	
- _Add_ dishes set to the utensils set with update method.
	- Should _list_ the elements from both lists when using the _for loop_. 
```
utensils.update(dishes)
```

- _Join_ two sets together _creating_ a new set.
```
dinner_table = utensils.union(dishes)
```

- _Look_ for the difference _between_ two sets.
```
print(utensils.difference(dishes))
```

- _Check_ to see if _both_ sets has something in common.
```
print(utensils.intersection(dishes))
> knife
```	

