# Pathlib

~~~ 
import pathlib

# Gives me the absolute path to the file I'm working on
this_file = pathlib.Path(__file__).resolve()

# Tells me which folder this file is in:
this_dir = pathlib.Path(".").resolve()

# How we can use paths to find a new path
doc = this_dir / 'notes' / 'doc.txt'

# If I need the parent directory for a path
parent = doc.parent
#Check if it is a directory
doc.is_dir()

# Create a directory
parent.mkdir(exist_ok=True)

# Write data into a file
doc.write_text("Hello world")

# Check if it is a file or if it exists
doc.is_file()
doc.exists()


~~~
